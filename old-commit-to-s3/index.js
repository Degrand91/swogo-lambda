var AWS = require('aws-sdk');
var GitHubApi = require('github');
var configFileLocation = {Bucket: 'swogo-api', Key: 'config.json'};     //todo define config file/location
var AWScredentials = { region: "eu-west-1", accessKeyId: "AKIAI5BBR6VXXCKPNERA",  secretAccessKey: "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" };
var s3client = new AWS.S3(AWScredentials);
var github = new GitHubApi({ protocol: "https", host: "api.github.com" , version: '3.0.0', debug: false });
github.authenticate({ type: 'oauth', token: "151b47bfdf3e82ae2410a800b138d02531dce93b"});   //todo change token with another github account


exports.handler = function(event, context, callback) {
    var s3Bucket;
    var githubEvent = event.Records[0].Sns;
    var messageAttributes = githubEvent.MessageAttributes;
    var isPushEvent = (messageAttributes.hasOwnProperty('X-Github-Event')) && (messageAttributes['X-Github-Event'].Value == "push");
    if (!isPushEvent) return false;

    var eventInfo = JSON.parse(githubEvent.Message);
    var re = new RegExp(/([^\/]*)/);
    var found = re.exec(eventInfo.repository.full_name);
    var gitConf = {owner: found[0], repo :eventInfo.repository.name ,sha: eventInfo.head_commit.id };

    getConfig(configFileLocation,getBucket );

    function getConfig(params,complete){
        s3client.getObject(params, function(err, data){
            if (err) return "config not found!";
            var bucketsConfig = JSON.parse(data.Body.toString());
            complete(bucketsConfig);
        });
    }
    function getBucket (configFile){
        if (!configFile) callback("missing config File!");
        s3Bucket = configFile[gitConf.repo];
        if (!s3Bucket) callback("missing repository on the config file");
        github.repos.getCommit(gitConf, executeCommit);
    }
    function executeCommit(err, commit) {
        if (err) return err;
        parseCommit(commit, function done(err) {
            if (err)  callback ("Parsing the commit failed: " + err);
            else      callback(null,"Commit parsed and synced successfully.");
        });
    }
    function parseCommit(commitObject, complete){
        if ((commitObject.files) && (commitObject.files.length >0)) {
            forEachFile(commitObject.files, 0, complete);
        } else {
            console.log("Commit at " + commitObject.html_url + " had no files. Exiting.");
            complete(new Error('No files in commit object'));
        }
    }
    function forEachFile(files, fileIndex, complete){
        var file = files[fileIndex];
        if (!file) complete();
        else {
            executeFileStatus(file, function (){
                fileIndex++;
                forEachFile(files, fileIndex, complete);
            })
        }
    }
    function executeFileStatus (file, complete){
        if (file.status == "removed") {
            s3delete(file, complete);
        } else if (file.status = "renamed"){
            s3rename(file, complete);
        } else {
            s3put(file, complete);
        }
    }
    function s3rename(file, complete){
        s3delete(file,function(){
            s3put(file,complete)
        })
    }
    function s3delete(file, complete){
        var filename = file.filename;
        var params = { Bucket: s3Bucket, Key: filename };
        console.log("Deleting ", filename);
        s3client.deleteObject(params, function (err){
            if (err) console.log("Couldn't delete " + filename + ": " + err);
            else  console.log("Deleted " + filename + " from " + s3Bucket);
            complete();
        });
    }
    function s3put(file, complete){
        download(file,function(err,result){
            if (err) console.log("Couldn't download " + file.filename + ": " + err);
            else  console.log("Downloaded " + file.filename + " from " + gitConf.repo);
            store(file,result,complete);
        })
    }
    function download(file,complete){
        console.log("Downloading " + file.filename);
        var params = { owner: gitConf.owner, repo: gitConf.repo, sha: file.sha};
        github.gitdata.getBlob(params, complete);
    }
    function store(file,body,complete){
        var filename = file.filename;
        console.log("Storing " + filename);
        var blob = new Buffer(body.content, 'base64');
        blob = blob.toString('utf-8');
        var putparams = { Bucket: s3Bucket, Key: filename, Body: blob,ACL: "public-read"};
        if(filename.indexOf(".js") >= 0)   putparams['ContentType'] = "application/javascript";
        if(filename.indexOf(".json") >= 0) putparams['ContentType'] = "application/json";
        if(filename.indexOf(".css") >= 0)  putparams['ContentType'] = "text/css";
        s3client.putObject(putparams, function(err){
            if (err) console.log("Couldn't store " + filename + " in bucket " + s3Bucket + "; " + err);
            else     console.log("Saved " + filename + " to " + s3Bucket + " successfully.");
            complete()
        });
    }
};