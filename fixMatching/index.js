"use strict";
let aws = require("aws-sdk");
let request = require('request');
let AWScredentials = { region: "eu-west-1", accessKeyId: "xxx",  secretAccessKey: "xxx" };
let s3client = new aws.S3(AWScredentials);
/*
let test = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-west-1:001247609713:GitHubDeploy:822337d9-4432-43d5-bf0f-20aedcbf886b",
            "Sns": {
                "Type": "Notification",
                "MessageId": "a44350a8-3c3b-5a9f-ab00-01312769091b",
                "TopicArn": "arn:aws:sns:eu-west-1:001247609713:GitHubDeploy",
                "Subject": null,
                "Message": "{\"ref\":\"refs/heads/master\",\"before\":\"fad3fc381da69bf68af703cc072f827f01c99e95\",\"after\":\"dffb4e2add6f71879597210a7fad25b30df651b3\",\"created\":false,\"deleted\":false,\"forced\":false,\"base_ref\":null,\"compare\":\"https://github.com/swogger/bundlesUI.3.0/compare/fad3fc381da6...dffb4e2add6f\",\"commits\":[{\"id\":\"dffb4e2add6f71879597210a7fad25b30df651b3\",\"tree_id\":\"df7922e8a69f663db2a16c239f2cf208ff4a0a6e\",\"distinct\":true,\"message\":\"api test\",\"timestamp\":\"2017-05-19T17:10:43+01:00\",\"url\":\"https://github.com/swogger/bundlesUI.3.0/commit/dffb4e2add6f71879597210a7fad25b30df651b3\",\"author\":{\"name\":\"=\",\"email\":\"stefano@swogo.com\",\"username\":\"Degrand\"},\"committer\":{\"name\":\"=\",\"email\":\"stefano@swogo.com\",\"username\":\"Degrand\"},\"added\":[],\"removed\":[],\"modified\":[\"api-client.js\"]}],\"head_commit\":{\"id\":\"dffb4e2add6f71879597210a7fad25b30df651b3\",\"tree_id\":\"df7922e8a69f663db2a16c239f2cf208ff4a0a6e\",\"distinct\":true,\"message\":\"api test\",\"timestamp\":\"2017-05-19T17:10:43+01:00\",\"url\":\"https://github.com/swogger/bundlesUI.3.0/commit/dffb4e2add6f71879597210a7fad25b30df651b3\",\"author\":{\"name\":\"=\",\"email\":\"stefano@swogo.com\",\"username\":\"Degrand\"},\"committer\":{\"name\":\"=\",\"email\":\"stefano@swogo.com\",\"username\":\"Degrand\"},\"added\":[],\"removed\":[],\"modified\":[\"api-client.js\"]},\"repository\":{\"id\":78863040,\"name\":\"bundlesUI.3.0\",\"full_name\":\"swogger/bundlesUI.3.0\",\"owner\":{\"name\":\"swogger\",\"email\":\"dimitar@swogo.com\",\"login\":\"swogger\",\"id\":4025917,\"avatar_url\":\"https://avatars2.githubusercontent.com/u/4025917?v=3\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/swogger\",\"html_url\":\"https://github.com/swogger\",\"followers_url\":\"https://api.github.com/users/swogger/followers\",\"following_url\":\"https://api.github.com/users/swogger/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/swogger/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/swogger/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/swogger/subscriptions\",\"organizations_url\":\"https://api.github.com/users/swogger/orgs\",\"repos_url\":\"https://api.github.com/users/swogger/repos\",\"events_url\":\"https://api.github.com/users/swogger/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/swogger/received_events\",\"type\":\"User\",\"site_admin\":false},\"private\":true,\"html_url\":\"https://github.com/swogger/bundlesUI.3.0\",\"description\":null,\"fork\":false,\"url\":\"https://github.com/swogger/bundlesUI.3.0\",\"forks_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/forks\",\"keys_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/keys{/key_id}\",\"collaborators_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/collaborators{/collaborator}\",\"teams_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/teams\",\"hooks_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/hooks\",\"issue_events_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/issues/events{/number}\",\"events_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/events\",\"assignees_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/assignees{/user}\",\"branches_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/branches{/branch}\",\"tags_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/tags\",\"blobs_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/git/blobs{/sha}\",\"git_tags_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/git/tags{/sha}\",\"git_refs_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/git/refs{/sha}\",\"trees_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/git/trees{/sha}\",\"statuses_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/statuses/{sha}\",\"languages_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/languages\",\"stargazers_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/stargazers\",\"contributors_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/contributors\",\"subscribers_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/subscribers\",\"subscription_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/subscription\",\"commits_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/commits{/sha}\",\"git_commits_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/git/commits{/sha}\",\"comments_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/comments{/number}\",\"issue_comment_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/issues/comments{/number}\",\"contents_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/contents/{+path}\",\"compare_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/compare/{base}...{head}\",\"merges_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/merges\",\"archive_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/{archive_format}{/ref}\",\"downloads_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/downloads\",\"issues_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/issues{/number}\",\"pulls_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/pulls{/number}\",\"milestones_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/milestones{/number}\",\"notifications_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/notifications{?since,all,participating}\",\"labels_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/labels{/name}\",\"releases_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/releases{/id}\",\"deployments_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0/deployments\",\"created_at\":1484322549,\"updated_at\":\"2017-01-13T15:52:08Z\",\"pushed_at\":1495210257,\"git_url\":\"git://github.com/swogger/bundlesUI.3.0.git\",\"ssh_url\":\"git@github.com:swogger/bundlesUI.3.0.git\",\"clone_url\":\"https://github.com/swogger/bundlesUI.3.0.git\",\"svn_url\":\"https://github.com/swogger/bundlesUI.3.0\",\"homepage\":null,\"size\":356,\"stargazers_count\":0,\"watchers_count\":0,\"language\":\"JavaScript\",\"has_issues\":true,\"has_projects\":true,\"has_downloads\":true,\"has_wiki\":true,\"has_pages\":false,\"forks_count\":0,\"mirror_url\":null,\"open_issues_count\":0,\"forks\":0,\"open_issues\":0,\"watchers\":0,\"default_branch\":\"master\",\"stargazers\":0,\"master_branch\":\"master\"},\"pusher\":{\"name\":\"Degrand\",\"email\":\"stefano@swogo.com\"},\"sender\":{\"login\":\"Degrand\",\"id\":11030014,\"avatar_url\":\"https://avatars0.githubusercontent.com/u/11030014?v=3\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/Degrand\",\"html_url\":\"https://github.com/Degrand\",\"followers_url\":\"https://api.github.com/users/Degrand/followers\",\"following_url\":\"https://api.github.com/users/Degrand/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/Degrand/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/Degrand/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/Degrand/subscriptions\",\"organizations_url\":\"https://api.github.com/users/Degrand/orgs\",\"repos_url\":\"https://api.github.com/users/Degrand/repos\",\"events_url\":\"https://api.github.com/users/Degrand/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/Degrand/received_events\",\"type\":\"User\",\"site_admin\":false}}",
                "Timestamp": "2017-05-19T16:10:58.639Z",
                "SignatureVersion": "1",
                "Signature": "Pfi0/zkDI+J/NPunxdKF7i+QLZeFyeZAwDsCsQjHIsg5uum2e29CDhKhrqU8y/f3YEt2lcUzj9Bmhquiti4wDP4s7WL5k/4JL/XfAVNrAXBqHZS1l2SwEVvEFU7td8TYHpIUNYuWrZx7POHGiDkzQoANjKyx8NgnzYkUxh16iwHQ0jN61G5V54E38NUiIanHJSuqSzVLjQp0IHabxFBavgURyAfqD612OAKlFmdZ1AAG8a5S4TnatqVt998BT38MvCiuW4aYg8JyP8GHG3f7yFuCwdn+Chm4eF7rg7JA3VYsqh+VANtFJ07Xpdudv9KvnyRjnYvBcuEQrTLWR6wBsQ==",
                "SigningCertUrl": "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                "UnsubscribeUrl": "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:001247609713:GitHubDeploy:822337d9-4432-43d5-bf0f-20aedcbf886b",
                "MessageAttributes": {
                    "X-Github-Event": {
                        "Type": "String",
                        "Value": "push"
                    }
                }
            }
        }
    ]
};
*/

let test = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-west-1:001247609713:GitHubDeploy:50248846-b569-4626-b028-117ba1d1d17b",
            "Sns": {
                "Type": "Notification",
                "MessageId": "8cf32bd5-2400-5f16-91b4-7a41d88418ee",
                "TopicArn": "arn:aws:sns:eu-west-1:001247609713:GitHubDeploy",
                "Subject": null,
                "Message": "{\"ref\":\"refs/heads/master\",\"before\":\"ab21aa967e6e20238472b2b24acb822a9f07a0ed\",\"after\":\"0edeba6ccf368475e1b5d723f804a361b213a05c\",\"created\":false,\"deleted\":false,\"forced\":false,\"base_ref\":null,\"compare\":\"https://github.com/swogger/bundlesUI.3.0.integrations/compare/ab21aa967e6e...0edeba6ccf36\",\"commits\":[{\"id\":\"dfb19ea3ebf50fbe11b9317e6052799950a4fd24\",\"tree_id\":\"5357c8210cc991ae42d344af4d2fb8f9048373e3\",\"distinct\":true,\"message\":\"Added bundles pdp\",\"timestamp\":\"2017-05-16T14:10:03+01:00\",\"url\":\"https://github.com/swogger/bundlesUI.3.0.integrations/commit/dfb19ea3ebf50fbe11b9317e6052799950a4fd24\",\"author\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"committer\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"added\":[\"alzaCz/A/client.js\"],\"removed\":[\"alzaCz/A/swogo.js\"],\"modified\":[\"alzaCz/A/style.css\",\"alzaCz/swogo.js\"]},{\"id\":\"0edeba6ccf368475e1b5d723f804a361b213a05c\",\"tree_id\":\"7e580f77e5a82faaf29478f34ce098a2fa6bb157\",\"distinct\":true,\"message\":\"Merge branch 'master' of https://github.com/swogger/bundlesUI.3.0.integrations\",\"timestamp\":\"2017-05-16T14:10:11+01:00\",\"url\":\"https://github.com/swogger/bundlesUI.3.0.integrations/commit/0edeba6ccf368475e1b5d723f804a361b213a05c\",\"author\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"committer\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"added\":[\"default/client.js\"],\"removed\":[\"default/mediamarktNlFull.js\"],\"modified\":[\"alza.cz/A/client.js\",\"bt.com/A/client.js\",\"bt.com/A/style.css\",\"ochsnersport.ch/A/client.js\",\"phonehouse.es/A/client.js\",\"default/style.css\",\"default/swogo.js\",\"tool/uploadfile.js\"]}],\"head_commit\":{\"id\":\"0edeba6ccf368475e1b5d723f804a361b213a05c\",\"tree_id\":\"7e580f77e5a82faaf29478f34ce098a2fa6bb157\",\"distinct\":true,\"message\":\"Merge branch 'master' of https://github.com/swogger/bundlesUI.3.0.integrations\",\"timestamp\":\"2017-05-16T14:10:11+01:00\",\"url\":\"https://github.com/swogger/bundlesUI.3.0.integrations/commit/0edeba6ccf368475e1b5d723f804a361b213a05c\",\"author\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"committer\":{\"name\":\"Rodrigo Alves\",\"email\":\"rodrigo@swogo.com\",\"username\":\"RodrigoSwogo\"},\"added\":[\"default/client.js\"],\"removed\":[\"default/mediamarktNlFull.js\"],\"modified\":[\"alza.cz/A/client.js\",\"bt.com/A/client.js\",\"bt.com/A/style.css\",\"ochsnersport.ch/A/client.js\",\"phonehouse.es/A/client.js\",\"default/style.css\",\"default/swogo.js\",\"tool/uploadfile.js\"]},\"repository\":{\"id\":85216484,\"name\":\"bundlesUI.3.0.integrations\",\"full_name\":\"swogger/bundlesUI.3.0.integrations\",\"owner\":{\"name\":\"swogger\",\"email\":\"dimitar@swogo.com\",\"login\":\"swogger\",\"id\":4025917,\"avatar_url\":\"https://avatars2.githubusercontent.com/u/4025917?v=3\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/swogger\",\"html_url\":\"https://github.com/swogger\",\"followers_url\":\"https://api.github.com/users/swogger/followers\",\"following_url\":\"https://api.github.com/users/swogger/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/swogger/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/swogger/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/swogger/subscriptions\",\"organizations_url\":\"https://api.github.com/users/swogger/orgs\",\"repos_url\":\"https://api.github.com/users/swogger/repos\",\"events_url\":\"https://api.github.com/users/swogger/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/swogger/received_events\",\"type\":\"User\",\"site_admin\":false},\"private\":true,\"html_url\":\"https://github.com/swogger/bundlesUI.3.0.integrations\",\"description\":null,\"fork\":false,\"url\":\"https://github.com/swogger/bundlesUI.3.0.integrations\",\"forks_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/forks\",\"keys_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/keys{/key_id}\",\"collaborators_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/collaborators{/collaborator}\",\"teams_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/teams\",\"hooks_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/hooks\",\"issue_events_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/issues/events{/number}\",\"events_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/events\",\"assignees_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/assignees{/user}\",\"branches_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/branches{/branch}\",\"tags_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/tags\",\"blobs_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/git/blobs{/sha}\",\"git_tags_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/git/tags{/sha}\",\"git_refs_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/git/refs{/sha}\",\"trees_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/git/trees{/sha}\",\"statuses_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/statuses/{sha}\",\"languages_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/languages\",\"stargazers_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/stargazers\",\"contributors_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/contributors\",\"subscribers_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/subscribers\",\"subscription_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/subscription\",\"commits_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/commits{/sha}\",\"git_commits_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/git/commits{/sha}\",\"comments_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/comments{/number}\",\"issue_comment_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/issues/comments{/number}\",\"contents_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/contents/{+path}\",\"compare_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/compare/{base}...{head}\",\"merges_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/merges\",\"archive_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/{archive_format}{/ref}\",\"downloads_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/downloads\",\"issues_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/issues{/number}\",\"pulls_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/pulls{/number}\",\"milestones_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/milestones{/number}\",\"notifications_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/notifications{?since,all,participating}\",\"labels_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/labels{/name}\",\"releases_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/releases{/id}\",\"deployments_url\":\"https://api.github.com/repos/swogger/bundlesUI.3.0.integrations/deployments\",\"created_at\":1489680904,\"updated_at\":\"2017-04-10T09:52:29Z\",\"pushed_at\":1494940227,\"git_url\":\"git://github.com/swogger/bundlesUI.3.0.integrations.git\",\"ssh_url\":\"git@github.com:swogger/bundlesUI.3.0.integrations.git\",\"clone_url\":\"https://github.com/swogger/bundlesUI.3.0.integrations.git\",\"svn_url\":\"https://github.com/swogger/bundlesUI.3.0.integrations\",\"homepage\":null,\"size\":1730,\"stargazers_count\":0,\"watchers_count\":0,\"language\":\"JavaScript\",\"has_issues\":true,\"has_projects\":true,\"has_downloads\":true,\"has_wiki\":true,\"has_pages\":false,\"forks_count\":0,\"mirror_url\":null,\"open_issues_count\":0,\"forks\":0,\"open_issues\":0,\"watchers\":0,\"default_branch\":\"master\",\"stargazers\":0,\"master_branch\":\"master\"},\"pusher\":{\"name\":\"RodrigoSwogo\",\"email\":\"rodrigo@swogo.com\"},\"sender\":{\"login\":\"RodrigoSwogo\",\"id\":28646822,\"avatar_url\":\"https://avatars1.githubusercontent.com/u/28646822?v=3\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/RodrigoSwogo\",\"html_url\":\"https://github.com/RodrigoSwogo\",\"followers_url\":\"https://api.github.com/users/RodrigoSwogo/followers\",\"following_url\":\"https://api.github.com/users/RodrigoSwogo/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/RodrigoSwogo/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/RodrigoSwogo/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/RodrigoSwogo/subscriptions\",\"organizations_url\":\"https://api.github.com/users/RodrigoSwogo/orgs\",\"repos_url\":\"https://api.github.com/users/RodrigoSwogo/repos\",\"events_url\":\"https://api.github.com/users/RodrigoSwogo/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/RodrigoSwogo/received_events\",\"type\":\"User\",\"site_admin\":false}}",
                "Timestamp": "2017-05-16T13:10:28.673Z",
                "SignatureVersion": "1",
                "Signature": "OEChrFNlXVqVsHlp23FnnG6CVtQ6rVji2yRAFBU7Nn+h+R+E6t87GBt/Ii68R+qAbBlLzt2hn4Ic3fRSHH/DQ8sGHlHuNpQcNA4qTkc++WY9J9OvbaGMmzEDp4MFtJwmjFidoIZu83wopaSMtGPKoHe+s2jVM+T3CoC6GYnwAaKMCw9yVzTrbrBfSsQFsD4VdBWUZmV9WuDG6IQq3G6QGY+5p+fc8uFI0YOILZSt7027k7oHrtv+KvKsJft0lOXmQfvcKzOmZFnmTWCqLkwAjPUZwhNsluHF9h7joEVtxa9AyMyHn5ePh2pe6xeBroA4sox7B/4H2Ax/rdxC9hERdQ==",
                "SigningCertUrl": "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                "UnsubscribeUrl": "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:001247609713:GitHubDeploy:50248846-b569-4626-b028-117ba1d1d17b",
                "MessageAttributes": {
                    "X-Github-Event": {
                        "Type": "String",
                        "Value": "push"
                    }
                }
            }
        }
    ]
};
handler(test,"",console.log);

//exports.handler = handler;

function handler(event, context, callback) {
    let sns = event.Records && event.Records.length && event.Records[0].Sns;
    let gitMessage = sns && sns.Message;
    let gitAction = sns && sns.MessageAttributes && sns.MessageAttributes["X-Github-Event"] && sns.MessageAttributes["X-Github-Event"].Value;

    if(gitAction === "push" && gitMessage){
        let gitData = JSON.parse(gitMessage);
        let repName = gitData.repository.name;
        if ( !gitData.commits || !gitData.commits[0] ) {
            callback(null, gitMessage);
        }
        executeCommit(repName,gitData.commits[0], callback);
    }else{
        callback(null, gitMessage);
    }
}
function executeCommit(repName, commit, done){
    let repParams = {};
    let fileList = commit.added.concat(commit.modified);
    switch(repName){
        case"bundlesUI.3.0":{
            repParams = {
                s3Bucket : "ui.swogo.com",
                s3Path : "default/compiler/",
                rootPath : "https://151b47bfdf3e82ae2410a800b138d02531dce93b:@api.github.com/repos/swogger/bundlesUI.3.0/contents/",
                allowedFiles : ["ui-client.js","api-client.js"]
            };
            break;
        }
        case"bundlesUI.3.0.integrations":{
            repParams = {
                s3Bucket : "ui.swogo.com",
                rootPath : "https://151b47bfdf3e82ae2410a800b138d02531dce93b:@api.github.com/repos/swogger/bundlesUI.3.0.integrations/contents/",
            };

            break;
        }
    }
    proccessRemovedFiles(commit.removed,repParams,function(){
        processFileList(fileList,repParams,function(){
            done(null,"complete");
        });
    });
}
function processFileList (fileList,repParams,complete) {
    if (!fileList.length) return complete();
    let fileName = fileList.shift();
    if (repParams.allowedFiles && repParams.allowedFiles.indexOf(fileName) < 0){
        return processFileList(fileList,repParams,complete);
    }
    let filePath = repParams.rootPath+fileName;
    getFile (filePath,function(err,fileRes) {
        if (err || !fileRes ) {throw new Error('Whoops, something went wrong!');}
        let fileContent = fileRes.body.toString();
        uploadFileOnS3(repParams,fileName,fileContent,function(){
            processFileList(fileList,repParams,complete);
        });
    });
}
function proccessRemovedFiles(files,repParams,complete){
    if (!files.length) return complete();
    let fileName = files.shift();
    if (repParams.allowedFiles && repParams.allowedFiles.indexOf(fileName) < 0){
        return proccessRemovedFiles(files,repParams,complete);
    }
    deleteFileOnS3(repParams,fileName,function(){
        proccessRemovedFiles(files,repParams,complete);
    });
}
function getFile (fileUrl,callback) {

    let options = {
        url: fileUrl,
        headers: {
            'Accept': 'application/vnd.github.VERSION.raw',
            'User-Agent': 'request module',
        },
        encoding: null
    };
    console.log("Downloading "+fileUrl);
    request(options, callback);
}
function uploadFileOnS3 (repParams,filePath,data,complete) {
    let params = {
        Bucket: repParams.s3Bucket,
        Key: (repParams.s3Path) ? repParams.s3Path+filePath : filePath,
        Body: data,
        ACL: "public-read"
    };
    if(filePath.indexOf(".js")>= 0) {
        params["ContentEncoding"] = "gzip";
        params['ContentType'] = "text/javascript; charset=UTF-8";
    }
    if (filePath.indexOf(".json") >= 0) params['ContentType'] = "application/json; charset=UTF-8";
    if (filePath.indexOf(".css") >= 0)  params['ContentType'] = "text/css; charset=UTF-8";
    console.log("Uploading ", params.Key);
    s3client.putObject(params, complete);
}
function deleteFileOnS3 (repParams,filePath,complete){
    let params = {
        Bucket: repParams.s3Bucket,
        Key: (repParams.s3Path) ? repParams.s3Path+filePath : filePath
    };
    console.log("Deleting ", params.Key);
    s3client.deleteObject(params, complete);
}