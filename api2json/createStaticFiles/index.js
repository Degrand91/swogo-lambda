"use strict";
var http = require("http");

function atob(str) {
    return (new Buffer(str, 'base64')).toString('binary');
}

/** Encode string */
function btoa(str) {
    let buffer;
    if (str instanceof Buffer) buffer = str;
    else buffer = new Buffer(str.toString(), 'binary');
    return buffer.toString('base64');
}

function api2json(event, context, callback) {
    var AWS = require('aws-sdk');
    AWS.config.update({
        region: "eu-west-1",
        accessKeyId: 'xxx',
        secretAccessKey: 'xxx'
    });
    var s3 = new AWS.S3();
    var filename = decodeURIComponent(event.Records[0].s3.object.key).replace("trigger/api2json/","");
    if(!filename) callback("client undefined",null);
    getActionLog(filename,processAction);


    function getActionLog(filename,cb){
        var s3Params = {
            Bucket: "swogo-api",
            Key: "trigger/api2json/"+filename
        };
        s3.getObject(s3Params, function (err, data) {
            if(err) return cb(err,null);
            var actionLists = JSON.parse(data.Body.toString());
            cb(null,actionLists);
        });
    }

    function processAction(err,actionLists){
        var queryIndex;
        var path = 'trigger/api2json/'+filename;
        for (var a = 0; a < actionLists.length; a++){
            var action = actionLists[a];
            if (action.status === 1 && action.retryCount < 10) {
                queryIndex = a;
            }
            if (queryIndex || queryIndex===0) break;
        }

        if (queryIndex === undefined) {
            updateFileExt(filename,callback);
        } else {
            sendApiRequest(actionLists,queryIndex,function(newActionList){
                uploadFile(path,newActionList,callback)
            })
        }
    }
    function sendApiRequest(actionLists,queryIndex,cb){
        var query = actionLists[queryIndex];
        var reqObj = {client:"",type:"bundles",matching:"pdp",skus:[],filters:{}};

        reqObj.matching = query.bundleType;
        reqObj.client = filename.replace(/\w+_/,"").replace(".open","");
        reqObj.skus = [query.productId];

        var options = {
            host: "api.swogo.com",
            //port: 8080,
            method: 'GET',
            path: btoa(encodeURIComponent(JSON.stringify(reqObj))),
        };

        var path = 'json/'+reqObj.client+'/'+query.bundleType+'/'+query.productId;
        http.get(options, function(res) {
            let body = '';
            res.on('data', function(d) {
                body += d;
            });
            res.on('end', function() {
                let decoded = decodeURIComponent(atob(body));
                processResponse(res,decoded)
            })
        });

        function processResponse(res,body){
            if (res.statusCode == 200) {
                uploadFile(path,JSON.parse(body),function(err,data){
                    query.status = 0;
                    query.complete = 1;
                    cb(actionLists);
                });
            } else if(actionLists[queryIndex].retryCount < 10){
                query.retryCount++;
                cb(actionLists);
            } else {
                query.status = 0;
                cb(actionLists);
            }
        }
    }
    function uploadFile(path,content,cb){
        var param = {
            Bucket: 'swogo-api',
            Key: path,
            ContentType: "application/json",
            Body: JSON.stringify(content),
            ACL:'public-read'
        };
        s3.putObject(param, function(){
            cb(null,"file update!");
        });
    }

    function updateFileExt(filename,cb){
        var clientName = filename.replace(/\w+_/,"").replace(".open","");
        var copyParams = {
            Bucket: "swogo-api",
            CopySource: "swogo-api/trigger/api2json/"+filename,
            Key: "trigger/api2json/logs/"+clientName+"/"+filename.replace(".open",".close")
        };
        var deleteParams = {
            Bucket: "swogo-api",
            Delete: {
                Objects: [{Key: "trigger/api2json/"+filename}]
            }
        };
        s3.copyObject(copyParams,function(err,data){
            s3.deleteObjects(deleteParams, cb);
        });
    }

    function getBody(res,done){
        var gunzip = require('zlib').createGunzip();
        var body = '';
        res.pipe(gunzip);
        gunzip.on('data', function (data) {
            body += data;
        });
        gunzip.on('end', function () {
            done(res,body);
        });
    }
}

setInterval(function(){
    //event.Records[0].s3.object.key

    var test = {
        "Records": [
            {
                "eventVersion": "2.0",
                "eventTime": "1970-01-01T00:00:00.000Z",
                "requestParameters": {
                    "sourceIPAddress": "127.0.0.1"
                },
                "s3": {
                    "configurationId": "testConfigRule",
                    "object": {
                        "eTag": "0123456789abcdef0123456789abcdef",
                        "sequencer": "0A1B2C3D4E5F678901",
                        "key": "trigger/api2json/laptops_mediamarktNl.open",
                        "size": 1024
                    },
                    "bucket": {
                        "arn": "arn:aws:s3:::mybucket",
                        "name": "sourcebucket",
                        "ownerIdentity": {
                            "principalId": "EXAMPLE"
                        }
                    },
                    "s3SchemaVersion": "1.0"
                },
                "responseElements": {
                    "x-amz-id-2": "EXAMPLE123/5678abcdefghijklambdaisawesome/mnopqrstuvwxyzABCDEFGH",
                    "x-amz-request-id": "EXAMPLE123456789"
                },
                "awsRegion": "us-east-1",
                "eventName": "ObjectCreated:Put",
                "userIdentity": {
                    "principalId": "EXAMPLE"
                },
                "eventSource": "aws:s3"
            }
        ]
    };
    api2json(test,null,console.log)

},1);