"use strict";
//let fs = require('fs');
//const SERVICE_ACCT_ID = 'importerscheduler@api-project-913629131893.swogo.com.iam.gserviceaccount.com';
const SERVICE_ACCT_ID = 'impotertest@test-a895e.iam.gserviceaccount.com';
const TIMEZONE = 'UTC+01:00';
//const CALENDAR_URL = 'https://calendar.google.com/calendar/ical/swogo.com_kdg6ql8ebrf92cdiubku10hevk%40group.calendar.google.com/public/basic.ics';
const CALENDAR_URL = 'https://calendar.google.com/calendar/ical/jqq0rsbfmrnc49f4s270ru69no%40group.calendar.google.com/public/basic.ics';
/*
const CALENDAR_ID = {
    'primary': 'swogo.com_kdg6ql8ebrf92cdiubku10hevk@group.calendar.google.com'
};
*/
const CALENDAR_ID = {
    'primary': '',
    'calendar-1': 'jqq0rsbfmrnc49f4s270ru69no@group.calendar.google.com'
};

let json = require('./key2.json');
let key = json.private_key;

module.exports.key = key;
module.exports.serviceAcctId = SERVICE_ACCT_ID;
module.exports.timezone = TIMEZONE;
module.exports.calendarId = CALENDAR_ID;
module.exports.calendarUrl = CALENDAR_URL;

