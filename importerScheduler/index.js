"use strict";

exports.handler = handler;

function handler(event, context, callback) {
    const config = require('./settings.js');
    const CalendarAPI = require('node-google-calendar');
    let cal = new CalendarAPI(config);
    let message = event.Records && event.Records.length && event.Records[0].Sns && JSON.parse(event.Records[0].Sns.Message);
    let colorId = (message.result.indexOf("ok") >= 0)? 1 : 2;

    let params = {
        'start': { 'dateTime': (message.start.substr(0,message.start.length-5))+'+00:00' },
        'end': { 'dateTime': (message.end.substr(0,message.end.length-5))+'+00:00' },
        'location': message.result,
        'summary': message.client,
        'status': 'confirmed',
        'description': message.result,
        'colorId': colorId
    };

    cal.Events.insert(config.calendarId["calendar-1"], params)

        .then(resp => {
            console.log('inserted event:');
            console.log(resp);
        })
        .catch(err => {
            console.log('Error: insertEvent-' + err);
        });

}

/*
handler(test,"",console.log);
let test = {
    "Records": [
        {
            "EventSource": "aws:sns",
            "EventVersion": "1.0",
            "EventSubscriptionArn": "arn:aws:sns:eu-west-1:001247609713:Importer-Execution-Event:f7452651-2869-4afc-877a-090f043e2682",
            "Sns": {
                "Type": "Notification",
                "MessageId": "b049b247-b1f0-5ead-b8f0-e3a579173d6f",
                "TopicArn": "arn:aws:sns:eu-west-1:001247609713:Importer-Execution-Event",
                "Subject": null,
                "Message": "{\"client\":\"harveynormanIe\",\"start\":\"2017-07-14T13:25:33.217Z\",\"end\":\"2017-07-14T13:25:56.229Z\",\"result\":\"ok - products: 6012\",\"products\":6012}",
                "Timestamp": "2017-07-14T13:27:03.828Z",
                "SignatureVersion": "1",
                "Signature": "lsHzTHZ11Evl9Ns+UyYPxDf9nTkp06aYUJS5vM0PlgrRUS3Vvzq7joGQeAyoIi6wctJBIggrwAYqCT+/QV+3s0+p4+m8mLe0m67q45HfaOBBfGGDJqRCnyv5l3/6iCHn6fzqxXcc6fnObrWAHN4isvZn59pZ4UKJzaIYnzKvV+4DaCSROg63bwzImRSmVTfETWlsY3pebVZphG/gTQX86BV8x+T/dSu6ojX2Ip2YXf991qlrtNTDL4UW8x4FKGTCUGErwiDagmaCo8v1i5iMziTM4euF1LfRZ3xeaascJ0DdueuHY+xv3iPN/3NvZizp7wLPanXFCuKI2QI4uqMp1A==",
                "SigningCertUrl": "https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
                "UnsubscribeUrl": "https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:001247609713:Importer-Execution-Event:f7452651-2869-4afc-877a-090f043e2682",
                "MessageAttributes": {}
            }
        }
    ]
};
*/
