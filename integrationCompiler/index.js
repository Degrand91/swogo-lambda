"use strict";
let uglify = require("uglify-js");
let request = require('request');
let aws = require("aws-sdk");
aws.config.update({region: "eu-west-1",accessKeyId: "xxxx",secretAccessKey: "xxxx"});
let s3Client = new aws.S3();

/*
handler({
    "Records": [
        {
            "eventVersion": "2.0",
            "eventSource": "aws:s3",
            "awsRegion": "eu-west-1",
            "eventTime": "2017-05-23T09:43:33.480Z",
            "eventName": "ObjectCreated:Put",
            "userIdentity": {
                "principalId": "AWS:AIDAJ446NWXOFQVCR4HJY"
            },
            "requestParameters": {
                "sourceIPAddress": "37.157.38.91"
            },
            "responseElements": {
                "x-amz-request-id": "2459E359011A1699",
                "x-amz-id-2": "MElFOjfR4WBQH/C17zfWUH4Ef6/eaGGbVudauk2NwXMaJJwVzgWAo665G97HNruZYy7fKFfoua0="
            },
            "s3": {
                "s3SchemaVersion": "1.0",
                "configurationId": "c412b203-1ca8-4c76-b462-d81f806f26fa",
                "bucket": {
                    "name": "ui.swogo.com",
                    "ownerIdentity": {
                        "principalId": "A3FIK8GC1X3RZB"
                    },
                    "arn": "arn:aws:s3:::ui.swogo.com"
                },
                "object": {
                    "key": "expertNo/A/client.js",
                    "size": 68811,
                    "eTag": "dc963c260ad1d7eae3707afdc0718452",
                    "sequencer": "00592404456A3228B9"
                }
            }
        }
    ]
},"",console.log);
*/
exports.handler = handler;

function handler(event, context, callback) {
    let gitFilesLocation = {
        "api" : "https://151b47bfdf3e82ae2410a800b138d02531dce93b:@api.github.com/repos/swogger/bundlesUI.3.0/contents/api-client.js",
        "ui" : "https://151b47bfdf3e82ae2410a800b138d02531dce93b:@api.github.com/repos/swogger/bundlesUI.3.0/contents/ui-client.js",
        "client" : "https://151b47bfdf3e82ae2410a800b138d02531dce93b:@api.github.com/repos/swogger/bundlesUI.3.0.integrations/contents/"
    };
    let key = event.Records[0].s3.object.key;
    gitFilesLocation.client = gitFilesLocation.client+key;
    compileScript(gitFilesLocation,callback);
}
function compileScript(gitFilesLocation, complete){
    let oldCoreReg = /this\.d(.+(\W)*)(\w+\W+)+this\.d\(el\)}\);/g;
    getFile (gitFilesLocation.api,function(apiErr,apiRes) {
        getFile (gitFilesLocation.ui,function(uiErr,uiRes) {
            getFile (gitFilesLocation.client,function(clientErr,clientRes) {
                if(apiErr || uiErr || clientErr || !apiRes || !uiRes || !clientRes) {
                    return complete(apiErr || uiErr || clientErr || "File error");
                }
                let scriptDelivery = 'this.d = function(s){var d=window.atob(s.substring(s.indexOf(",") + 1)),b=[],i,c;for(i=0;i<d.length;i++){c=d[i];b.push("%"+("00"+c.charCodeAt(0).toString(Math.pow(2,4))).slice(-2));}var o=decodeURIComponent(b.join(""));window.eval(o);};\n\
${content}.forEach(function(el){ this.d(el)});';
                let apiMin = uglify.minify(apiRes.body.toString(), {fromString: true});
                let uiMin = uglify.minify(uiRes.body.toString(), {fromString: true});
                let clientFile = clientRes.body.toString().replace(oldCoreReg,"");
                let content = [btoa(apiMin.code), btoa(uiMin.code)];
                let scriptClient = scriptDelivery.replace("${content}",JSON.stringify(content)) + "\n" + (clientFile.code || clientFile);
                let fileLocation = gitFilesLocation.client.match(/contents\S+/)[0].replace("contents/","").replace("client.js","swogo.js");
                uploadFileOnS3("ui.swogo.com",fileLocation,scriptClient,complete);
            });
        });
    });
}
function btoa(str) {
    let buffer;
    if (str instanceof Buffer) buffer = str;
    else buffer = new Buffer(str.toString(), "binary");
    return buffer.toString("base64");
}
function getFile (fileUrl,callback) {

    let options = {
        url: fileUrl,
        headers: {
            'Accept': 'application/vnd.github.VERSION.raw',
            'User-Agent': 'request module',
        },
        encoding: null
    };
    console.log("Downloading "+fileUrl);
    request(options, callback);
}
function uploadFileOnS3 (s3Bucket,filePath,data,complete) {
    let params = {
        Bucket: s3Bucket,
        Key: filePath,
        ContentType: "application/javascript; charset=UTF-8",
        //ContentEncoding: 'gzip',
        Body: data,
        ACL: "public-read"
    };
    console.log("uploading ", filePath);
    s3Client.putObject(params, complete);
}