var aws = require('aws-sdk');
var s3 = new aws.S3();

function saveSalesList(clientConfig, extension, uploaded){
    s3.putObject({
        Bucket:'xsellapp.com',
        Key: 'reporting/data/'+clientConfig.clientName+'_'+clientConfig.currentVariant+'_'+clientConfig.day + extension,
        ContentType: 'application/json',
        Body: JSON.stringify(clientConfig),
        ACL: 'authenticated-read'
    }, uploaded);
}

function parseClientConfig(err, data, complete) {
    if(err || !data || !data.Body){
        complete(err);
    } else{
        var configText = data.Body.toString();
        try{
            var config = JSON.parse(configText);
            complete(null, config);
        }catch(ex){
            complete(ex);
        }
    }
}

function arraymove(arr, fromIndex, toIndex) {
    var element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}

function objectToArray(soldItems, keys, ignoreNonSwogoSales){
    var arr = [];

    keys = keys || Object.keys(soldItems[0]).reverse();
    arr.push(keys);

    for(var salesCount = soldItems.length, i = 0; i < salesCount; i++){
        var item = soldItems[i];
        var data = [];
        data = keys.map(k => item[k] || 0);

        //var ignore = (ignoreNonSwogoSales) ? !item.hasSwogo : true;

        var ignore = (ignoreNonSwogoSales) ? item.host || item.swogoSale  : true;
        if(ignore) {
            arr.push(data);
        }

    }
    return arr;
}


function flattenSales(userSales,reportFields){
    var flat = [];
    var alpha = "QWERTYUIOPLKJHGFDSAZXCVBNM";
    var rid = alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)] +
        alpha[Math.floor(Math.random()*alpha.length)];

    for(var uSalesCount =  userSales.length, i = 0; i < uSalesCount; i++){

        var sales = userSales[i].sales;
        var saleIP = userSales[i].ip;
        var saleVar = userSales[i].variant;
        var saleUser = userSales[i].user;
        var saleTime = new Date(Math.floor(userSales[i].timestamp/100000));
        var saleTID =  userSales[i].transactionId;

        if(!sales) continue;

        for(var salesCount = sales.length, j = 0; j < salesCount; j++){
            try{


                if(sales[j] && sales[j].host && sales[j].host.length){
                    for(var h = 0; h < sales[j].host.length; h++){
                        if(!sales[j].host[h]) continue;
                        sales[j].host[h].hasSwogo =  userSales[i].swogoSales;
                        sales[j].host[h].transaction =  saleTID || 0;
                        sales[j].host[h].tid = (rid +""+i) || 0;
                        sales[j].host[h].variant = saleVar || 0;
                        sales[j].host[h].user = saleUser || 0;
                        sales[j].host[h].time = (saleTime && saleTime.toISOString()) || 0;
                        sales[j].host[h].ip = saleIP || 0;
                        sales[j].host[h].host = 1;
                        sales[j].host[h].origin = sales[j].host[h].origin || 'not bundle';
                        sales[j].host[h].id = sales[j].host[h].id ? sales[j].host[h].id.toString() : 0;
                        var newCat = sales[j].host[h].category ? sales[j].host[h].category.toString().replace(/([A-Z]+)/g, " $1").replace(/([a-z][A-Z])/g, " $1").toLowerCase() : "0";
                        sales[j].host[h].category =  newCat.replace(/\s\s+/g, ' ');
                        if( sales[j].host[h].swogoSale ) console.log( sales[j].host[h]);
                        flat.push(sales[j].host[h]);
                    }
                }

                if(sales[j] && sales[j].accessories && sales[j].accessories.length){
                    for(var a = 0; a < sales[j].accessories.length; a++){
                        if(!sales[j].accessories[a]) continue;
                        sales[j].accessories[a].hasSwogo =  userSales[i].swogoSales;
                        sales[j].accessories[a].transaction = saleTID || 0;
                        sales[j].accessories[a].tid =  (rid +""+i) || 0;
                        sales[j].accessories[a].variant = saleVar || 0;
                        sales[j].accessories[a].user = saleUser || 0;
                        sales[j].accessories[a].time =  (saleTime && saleTime.toISOString()) || 0;
                        sales[j].accessories[a].ip = saleIP || 0;
                        sales[j].accessories[a].host = 0;
                        sales[j].accessories[a].origin = sales[j].accessories[a].origin || 'not bundle';
                        sales[j].accessories[a].id = sales[j].accessories[a].id ? sales[j].accessories[a].id.toString() : 0;
                        var newCat =  sales[j].accessories[a].category ? sales[j].accessories[a].category.toString().replace(/([A-Z]+)/g, " $1").replace(/([a-z][A-Z])/g, " $1").toLowerCase() : "0";
                        sales[j].accessories[a].category = newCat.replace(/\s\s+/g, ' ');
                        flat.push(sales[j].accessories[a]);
                    }
                }
            }catch(ex){console.log(ex);}
        }

    }
    return flat;

}


exports.handler = (event, context, callback) => {

    //console.log(event);

    var parameters = {
        Bucket: event.Records[0].s3.bucket.name,
        Key:event.Records[0].s3.object.key
    };



    s3.getObject(parameters, function(err, data){

        parseClientConfig(err, data, function(error, config) {

            var fullListConfig = Object.assign({}, config);

            if(config.carts.length > 0){
                var cartItems = flattenSales(config.carts, config.reportFields);
                config.carts = objectToArray(cartItems, config.reportFields);
            }

            if(config.sales.length > 0){
                var soldItems = flattenSales(config.sales, config.reportFields);
                config.sales = objectToArray(soldItems, config.reportFields, true);
                fullListConfig.sales = objectToArray(soldItems, config.reportFields, false);
                fullListConfig.carts = [];
            }

            saveSalesList(config, '.flat', function(){
                saveSalesList(fullListConfig, '.full', callback);
            });

        });
    });
};