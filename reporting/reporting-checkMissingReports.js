var aws = require('aws-sdk');
var S3 = new aws.S3();
var date = new Date();
var day = date.getDay();
var parameters = {
    Bucket: 'xsellapp.com',
    Key: 'reporting/clients.config'
};

var offset = 0;


var now = new Date();
now.setDate(now.getDate() - offset);
var msPerDay = 24 * 60 * 60 * 1000;
var startOfDay = now - (now % msPerDay);
var lastDay = startOfDay - msPerDay;
var startDATE = new Date(lastDay);

function processClientsConfig(clients, complete){
    var clientNames = Object.keys(clients);
    var clientVariants = [];
    var errorReport = [];
    var checkedReport = [];
    var emptyReport = [];
    var counter = 0;

    clientNames.forEach(function(name, index){

        clients[name].variant = clients[name].variant || ["A"];
        clients[name].variant.forEach(function(currentVariant){

            var fileName = 'reporting/data/'+name + '_' + currentVariant + '_' + startDATE.toISOString().split('T')[0] + '.flat';
            var params = {
                Bucket: parameters.Bucket,
                Key: fileName
            }
            counter++;
            clientVariants.push(name + ' [ ' + currentVariant + ' ] ');

            S3.getObject(params , function (err, url) {

                if (err) errorReport.push(fileName)
                else{
                    var config = (url && url.Body) ? JSON.parse(url.Body.toString()) : null;
                    if( config && config.sales && ( !config.sales.length || config.sales.length <= 1)) emptyReport.push(fileName);
                    else checkedReport.push(fileName);
                }

                counter--;
                if(!counter) reportErrors(errorReport);
            });


        });

    });

    function reportErrors(errorRep){
        var email = {
            "Destination": {
                "ToAddresses": [
                    "dimitar@swogo.com", "lucy@swogo.com",
                ]
            },
            "Message": {
                "Subject": {
                    "Data": "Reporting Status: "+ startDATE,
                    "Charset": "UTF-8"
                },
                "Body": {
                    "Text": {
                        "Data":  " Reporting Date:" +startDATE +
                        "\n Clients:" +clientNames.join('\n, ') +
                        "\n Missing FIles: "+ errorRep.join('\n, ')+
                        "\n Empty Sales: "+ emptyReport.join('\n, ') +
                        "\n Confirmed: "+ checkedReport.join('\n, ')+"</p>",
                        "Charset": "UTF-8"
                    },
                    "Html": {
                        "Data":  " Reporting Date:" +startDATE +
                        "<br> Clients:<ol><li>" +clientVariants.join('</li><li>') +
                        "\n </li></ol><p style='color:red'>Missing Files: <ol><li>"+ errorRep.join('</li><li>') +
                        "\n </li></ol><p style='color:red'>Empty Sales: <ol><li>"+ emptyReport.join('</li><li>') +
                        "\n</li></ol></p> <p style='color:blue'>Confirmed: <ol><li>"+ checkedReport.join('</li><li>')+
                        "</li></ol></p>",
                        "Charset": "UTF-8"
                    }
                }
            },
            "Source": "dimitar@swogo.com"
        };

        S3.putObject({
            Bucket:'xsellapp.com',
            Key: 'emailer/'+startDATE.toISOString().split('T')[0] + '.email',
            ContentType: 'application/json',
            Body: JSON.stringify(email),
            ACL: 'authenticated-read'
        }, complete);
    }
}

exports.handler = (event, context, callback) => {

    S3.getObject(parameters, function parseConfig(err, data) {
        if(!err && data && data.Body){
            try{
                var clientListString = data.Body.toString();
                var clientList = JSON.parse(clientListString);
                processClientsConfig(clientList, callback);
            }catch(ex){
                callback(ex);
            }
        }else{
            callback(err);
        }
    });
};