var aws = require('aws-sdk');
var s3 = new aws.S3();

function saveSalesList(clientConfig, extension, uploaded){
    s3.putObject({
        Bucket:'xsellapp.com',
        Key: 'reporting/data/'+clientConfig.clientName+'_'+clientConfig.currentVariant+'_'+clientConfig.day + extension,
        ContentType: 'application/json',
        Body: JSON.stringify(clientConfig),
        ACL: 'authenticated-read'
    }, uploaded);
}

function parseClientConfig(err, data, complete) {
    if(err || !data || !data.Body){
        complete(err);
    } else{
        var configText = data.Body.toString();

        try{
            var config = JSON.parse(configText);
            complete(null, config);
        }catch(ex){
            complete(ex);
        }
    }
}

function updateBundles(userData, payload, origin) {

    if(payload && payload.host) {
        payload.host.origin = origin;
        userData.hosts.push(payload.host);
    }
    if(payload && payload.accessories && payload.accessories.length){
        payload.accessories = payload.accessories.map(function(a){
            a.origin = origin;
            return a;
        });
        userData.accessories = userData.accessories.concat(payload.accessories);
    }
}

function processSale(userData, timestamp, payload) {

    var skipEmptyTransactions = !userData
        || ( userData && userData.basket && userData.basket.length === 0 )
    //	|| ( userData && userData.accessories && userData.accessories.length === 0 );	// comment  to get all records


    if (skipEmptyTransactions) return;

    var basketLen = userData.basket.length;
    userData.timestamp = 0;

    while (basketLen--) {

        var basketItem = userData.basket[basketLen];
        var hostLen = userData.hosts.length;
        var accLen = userData.accessories.length;

        while (accLen--) {
            var accessory = userData.accessories[accLen];
            if (basketItem.id === accessory.id && basketItem.swogoSale !== 1) {
                basketItem.category = accessory.category || "?";
                userData.basket[basketLen].origin = accessory.origin;
                userData.basket[basketLen].swogoSale = 1;
                if (!userData.swogoSale) userData.swogoSale = 1;
                else userData.swogoSale++;
                hasSwogoSale = 1;
            }
        }
        // if marked as swogoSale it cannot be host. it lives in accessories array.
        while (hostLen--) {
            var host = userData.hosts[hostLen];
            if (basketItem.id === host.id) {
                basketItem.category = host.category || "?";
                userData.basket[basketLen].origin = host.origin;

                if(userData.basket[basketLen].swogoSale){
                    userData.basket[basketLen].host = 0;
                }else{
                    userData.basket[basketLen].host = 1;
                }
            }
        }


    }
    userData.timestamp = timestamp;
    if (userData.basket.length){
        userData.sales.push(userData.basket);
    }

    userData.basket = [];
    userData.hosts = [];
    userData.transactionId = payload
}

function processFinalData(events) {
    var userHistory = {};
    var eventLength = (events && events.length) || 0;

    while (eventLength--) {
        var event = events[eventLength];
        if (!userHistory[event.user]) userHistory[event.user] = {events: []};
        userHistory[event.user].events.push(event);
    }
    return userHistory;
}

function splitBasketByHost(sales){

    var saleCount = sales.length;

    while (saleCount--) {
        var sale = sales[saleCount];
        var host = [];
        var accessories = [];
        var prodCount = sale.length;
        while (prodCount--) {
            var product = sale[prodCount];
            if(product.host) host.push(product);
            else accessories.push(product);
        }
        sales[saleCount] = {host:host, accessories:accessories};
    }
    return sales;
}

function buildCartList(userEvents) {

    var totalCarts = [];
    var eventsLength = userEvents.length || 0;

    for (var i = 0; i < eventsLength; i++) {
        var event = userEvents[i];
        if(event.type === "bundleList"){
            event.payload.host = [event.payload.host];

            totalCarts.push({
                timestamp: event.timestamp ,
                user: event.user,
                ip: event.ip,
                variant:event.variant,
                sales: [event.payload], // only one per record , as opposed to sales
                transactionId: event.transactionId || ''
            });
        }
    }

    return totalCarts;
}


function buildUserData(user){
    var eventsLength = user.events && user.events.length || 0;
    var userData = {
        sales: [],
        hosts: [],
        accessories: [],
        basket: []
    };

    for (var i = 0; i < eventsLength; i++) {
        var event = user.events[i];
        switch (event.type) {
            case "bundleList":{
                updateBundles(userData, event.payload, event.origin || 'unknown');
                break;}
            case "basketList":{
                userData.basket = event.payload;
                break;}
            case "payment":{
                processSale(userData, event.timestamp, event.payload);
                break;}
        }
    }

    userData.sales = splitBasketByHost(userData.sales);
    return userData;
}

function buildSalesList(userEvents) {

    var userNames = Object.keys(userEvents);
    var usersCount = userNames.length;
    var totalSales = [];

    while (usersCount--) {

        var user = (userEvents[userNames[usersCount]]);
        var userData = buildUserData(user);

        if (userData.sales.length) {
            if(!userData.timestamp) console.log(JSON.stringify(userData));
            totalSales.push({
                timestamp: userData.timestamp ,
                user: user.events[0].user,
                ip: user.events[0].ip,
                variant: user.events[0].variant,
                swogoSales: userData.swogoSale || 0,
                sales: userData.sales,
                transactionId: userData.transactionId || ''
            });
        }
    }
    return totalSales;
}

exports.handler = (event, context, callback) => {

    var parameters = {
        Bucket: event.Records[0].s3.bucket.name,
        Key:event.Records[0].s3.object.key
    };
    //console.log(JSON.stringify(event));
    s3.getObject(parameters, function(err, data){

        parseClientConfig(err, data, function(error, config) {

            var records = config.data;
            config.data = ['cleaned by previous function to reduce space'];

            if(config.reports && (config.reports.indexOf('sales') >=0) ){
                var userEvents = processFinalData(records)
                config.sales = buildSalesList(userEvents);
            }

            if(config.reports && (config.reports.indexOf('carts') >=0) ){
                config.carts = buildCartList(records);
            }

            saveSalesList(config, '.sales', callback);

        });
    });
};