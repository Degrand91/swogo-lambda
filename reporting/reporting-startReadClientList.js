var aws = require('aws-sdk');
var S3 = new aws.S3();
var parameters = {
    Bucket: 'xsellapp.com',
    Key: 'reporting/clients.config'
};


const OFFSET = 0;


function makeInitConfig(clientData, variant, uploaded){
    clientData.day =  (new Date(clientData.startDate/100000)).toISOString().split('T')[0];
    clientData.currentVariant = variant;
    var key = 'reporting/data/'+clientData.clientName+'_'+variant+'_'+clientData.day + '.init';

    S3.putObject({
        Bucket:'xsellapp.com',
        Key: key,
        ContentType: 'application/json',
        Body: JSON.stringify(clientData),
        ACL: 'authenticated-read'
    }, uploaded);
}

function processClientsConfig(clients, complete){

    var date = new Date();
    var day = date.getDay();


    var now = new Date();
    now.setDate(now.getDate() - OFFSET);
    var msPerDay = 24 * 60 * 60 * 1000;
    var startOfDay = now - (now % msPerDay);
    var lastDay = startOfDay - msPerDay;
    var lastMonth = now.setDate(now.getDate()-30);

    var clientNames = Object.keys(clients);
    var errors = [];

    //clientNames = ["cdiscountCom"]; // use for testing purposes

    function initReport(clientName){
        var clientCount = 0;
        clients[clientName].clientName = clientName;
        clients[clientName].tableName = "sales_" + clientName.toLowerCase().replace('.','');
        clients[clientName].startDate = lastDay *  100000;
        clients[clientName].endDate = startOfDay * 100000;
        clients[clientName].deleteDate =lastMonth * 100000;
        clients[clientName].retry = 1000;
        clients[clientName].variant = clients[clientName].variant || ["A"];
        clientCount += clients[clientName].variant.length;

        function checkMakeFile(err, ok){
            clientCount--;
            if(err) errors.push(clientName +':'+ err.message);
            if(clientCount <= 0 ) complete(errors.length?errors.join(','):null, clientNames.length);
        }

        for(var v = 0; v < clients[clientName].variant.length; v++){

            makeInitConfig(clients[clientName], clients[clientName].variant[v], checkMakeFile);

        }
    }

    if(!clientNames.length) complete(null, 0);
    clientNames.forEach(initReport);
}

exports.handler = (event, context, callback) => {

    S3.getObject(parameters, parseConfig);

    function parseConfig(err, data) {
        if(!err && data && data.Body){
            try{
                var clientListString = data.Body.toString();
                var clientList = JSON.parse(clientListString);
                processClientsConfig(clientList, callback);
            }catch(ex){
                callback(ex);
            }
        }else{
            callback(err);
        }
    }
};