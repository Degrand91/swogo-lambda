var aws = require('aws-sdk');
var S3 = new aws.S3();
var dynamodb = new aws.DynamoDB();
var docClient = new aws.DynamoDB.DocumentClient();
const MAX_CAPACITY = 450;
exports.handler = main;


function main(event, context, callback){

    var parameters = {
        Bucket: event.Records[0].s3.bucket.name,
        Key:event.Records[0].s3.object.key
    };


    function prepareDinamoDB(error, config){
        if(error) callback(error, config);

        function saveSalesData(readError, data){
            if(readError) config.error.push(readError);
            config.data = data;
            updateInitConfig(config, ".records", callback);
            //setCapacity(config.tableName, capacity.write+2, capacity.read, console.log);
        }

        getCapacity(config.tableName, function checkReadyToRead(error, capacity){

            if(capacity.read >= MAX_CAPACITY){
                readSalesDB(config, saveSalesData );// todo add here the decision variable
            }else{
                setCapacity(config.tableName, capacity.write+1, MAX_CAPACITY, function retry(capErr, ok){
                    if(capErr) config.error?config.error.push(capErr):config.error = [capErr];
                    config.retry = config.retry - 1;
                    if(config.retry >= 0 ) setTimeout(updateInitConfig, 1000, config, ".init", callback);
                });
            }
        });

    }
    S3.getObject(parameters, function(err, data){
        parseClientConfig(err, data, prepareDinamoDB);
    });
}

function updateInitConfig(clientConfig, extension, uploaded){
    S3.putObject({
        Bucket:'xsellapp.com',
        Key: 'reporting/data/'+clientConfig.clientName+'_'+clientConfig.currentVariant+'_'+clientConfig.day + extension,
        ContentType: 'application/json',
        Body: JSON.stringify(clientConfig),
        ACL: 'authenticated-read'
    }, uploaded);
}

function parseClientConfig(err, data, complete) {
    if(err || !data || !data.Body){
        complete(err);
    } else{
        var configText = data.Body.toString();
        try{
            var config = JSON.parse(configText);
            complete(null, config);
        }catch(ex){
            complete(ex);
        }
    }
}

function getCapacity(tableName, complete){
    dynamodb.describeTable({TableName: tableName}, function (err, data){
        complete(err, {
            read: (data && data.Table && data.Table.ProvisionedThroughput.ReadCapacityUnits) || null,
            write: (data && data.Table && data.Table.ProvisionedThroughput.WriteCapacityUnits) || null
        });
    });
}

function setCapacity(tableName, writeCapacity, readCapacity, complete){
    var params = {
        "TableName": tableName,
        "ProvisionedThroughput": {}
    };
    if(readCapacity) params.ProvisionedThroughput.ReadCapacityUnits = readCapacity;
    if(writeCapacity) params.ProvisionedThroughput.WriteCapacityUnits = writeCapacity;
    dynamodb.updateTable(params, complete);
}

function readSalesDB(conf, complete) {

    var finalData = [];
    recursiveCall(conf, null);

    function recursiveCall(conf, LastEvaluatedKey) { // todo add and act on decision variable
        var reportQuery ='#var = :variant AND #ts BETWEEN :start AND :end';
        var deleteQuery ='#var = :variant AND #ts LT :del';

        var query = {
            TableName: conf.tableName,
            KeyConditionExpression: reportQuery,
            ExpressionAttributeNames:{
                "#var": "variant",
                "#ts": "timestamp"
            },
            ExpressionAttributeValues: {
                ':variant': conf.currentVariant,
                ':start': conf.startDate,
                ':end': conf.endDate//,
                // ':del': conf.deleteDate
            },
            ScanIndexForward: false,    // descending
            ExclusiveStartKey: LastEvaluatedKey
        };

        docClient.query(query, processPageData);
    }

    function processPageData(err, data) {
        if (err) {
            complete(err);
        } else {
            finalData = finalData.concat(data.Items);
            (!data.LastEvaluatedKey ) ? complete(null, finalData) : recursiveCall(conf, data.LastEvaluatedKey);
        }
    }
}